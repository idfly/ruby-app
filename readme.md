ruby docker image
=================

General purpose ruby image by idfly.ru.

Contains:

  * packages: imagemagick, nodejs
  * path extensions in order to run commands without `bundle exec`
